-- -------------------------add by cqw 20200624    新增流程管理目录----------------------start
insert into sys_menu(
menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
status,perms,icon,create_by,create_time,update_by,update_time,remark
) values
('流程管理', '0', '99', 'process',
null,   1, 'M', '0', '0', '', 'guide',   'admin', '2020-06-24 11-33-00', 'admin', '2018-03-16 11-33-00', '流程管理目录');
-- -------------------------add by cqw 20200624    新增流程管理目录----------------------end

-- ------------------------add by cqw 20200624    新增流程管理菜单----------------------start
insert into sys_menu(
menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
status,perms,icon,create_by,create_time,update_by,update_time,remark
) values
('流程管理', (select* from (select menu_id from sys_menu where path = 'workflow')tamp ), '1', 'workflowManage',
'process/workflow/index',   1, 'C', '0', '0', 'process:workflow:list', 'cascader',   'admin', '2020-06-24 11-33-00', 'admin', '2020-06-24 11-33-00', '流程管理菜单');
-- -----------------------add by cqw 20200624    新增流程管理菜单----------------------end


-- -------------------------add by cqw 20200624    新增产品管理目录----------------------start
insert into sys_menu(
menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
status,perms,icon,create_by,create_time,update_by,update_time,remark
) values
('产品管理', '0', '5', 'product',
null,   1, 'M', '0', '0', '', 'list',   'admin', '2020-06-24 11-33-00', 'admin', '2018-03-16 11-33-00', '产品管理目录');
-- -------------------------add by cqw 20200624    新增产品管理目录----------------------end


-- ------------------------add by cqw 20200624    新增库存管理菜单----------------------start
insert into sys_menu(
menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
status,perms,icon,create_by,create_time,update_by,update_time,remark
) values
('库存管理', (select * from (select menu_id from sys_menu where path = 'product')tamp ), '1', 'inventory',
'product/inventory/index',   1, 'C', '0', '0', 'product:inventory:list', 'chart',   'admin', '2020-06-24 11-33-00', 'admin', '2020-06-24 11-33-00', '库存管理菜单');
-- -----------------------add by cqw 20200624    新增库存管理菜单----------------------end


-- ------------------------add by cqw 20200624    供应商表----------------------start
drop table if exists pur_provider;
create table pur_provider (
    id                  bigint(20)      not null auto_increment    comment 'ID',
    provider_num        varchar(400)                               comment '供应商编号',
    provider_name       varchar(400)                               comment '供应商名称',
    provider_type       varchar(200)                               comment '供应商类型',
    status              char(2)                                    comment '审批状态(0:待提交,1:签审中,2:已批准)',
    valid_date          datetime                                   comment '有效期',
    pass_status         char(2)                                    comment '0:不合格，1:合格',
    create_by           varchar(64)                                comment '创建者',
    create_time         datetime                                   comment '创建时间',
    update_by           varchar(64)                                comment '更新者',
    update_time         datetime                                   comment '更新时间',
    remark              varchar(500)                               comment '备注',
    primary key (id)
) engine=innodb comment = '供应商表';
-- ------------------------add by cqw 20200624    供应商表----------------------end


-- ------------------------add by cqw 20200624    供应商联系人表----------------------start
drop table if exists pur_contact;
create table pur_contact (
    id                  bigint(20)      not null auto_increment    comment 'ID',
    contact_name        varchar(400)                               comment '联系人姓名',
    contact_pos         varchar(400)                               comment '联系人职位',
    contact_phone       varchar(200)                               comment '联系电话',
    provider_id         bigint(20)                                 comment '供应商ID',
    create_by           varchar(64)                                comment '创建者',
    create_time         datetime                                   comment '创建时间',
    update_by           varchar(64)                                comment '更新者',
    update_time         datetime                                   comment '更新时间',
    remark              varchar(500)                               comment '备注',
    primary key (id)
) engine=innodb comment = '供应商联系人表';
-- ------------------------add by cqw 20200624    供应商联系人表----------------------end


-- -------------------------add by cqw 20200624    采购管理目录----------------------start
insert into sys_menu(
    menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
    status,perms,icon,create_by,create_time,update_by,update_time,remark
) values
('采购管理', '0', '6', ' purchase',
 null,   1, 'M', '0', '0', '', 'shopping',   'admin', '2020-06-24 11-33-00', 'admin', '2018-03-16 11-33-00', '采购管理目录');
-- -------------------------add by cqw 20200624    采购管理目录----------------------end


-- ------------------------add by cqw 20200624    供应商管理菜单----------------------start
insert into sys_menu(
    menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
    status,perms,icon,create_by,create_time,update_by,update_time,remark
) values
('供应商管理', (select * from (select menu_id from sys_menu where path = 'purchase')tamp ), '1', 'provider',
 'purchase/provider/index',   1, 'C', '0', '0', 'purchase:provider:list', 'post',   'admin', '2020-06-24 11-33-00', 'admin', '2020-06-24 11-33-00', '供应商管理菜单');
-- -----------------------add by cqw 20200624    供应商管理菜单----------------------end


-- ------------------------add by cqw 20200627    供应商流程状态码表----------------------start
insert into sys_dict_type(
    dict_name,dict_type,status,create_by,create_time,update_by,update_time,remark
) values('供应商流程状态', 'provider_status','0', 'admin', '2018-03-16 11-33-00', 'admin','2018-03-16 11-33-00', '供应商流程状态');


insert into sys_dict_data(
dict_sort,dict_label,dict_value,dict_type,css_class,list_class,is_default,status,create_by,create_time,update_by,update_time,remark
) values
( 1,'待提交','0','provider_status','','','N','0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '待提交');

insert into sys_dict_data(
dict_sort,dict_label,dict_value,dict_type,css_class,list_class,is_default,status,create_by,create_time,update_by,update_time,remark
) values
( 2,'签审中','1','provider_status','','','N','0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '签审中');

insert into sys_dict_data(
dict_sort,dict_label,dict_value,dict_type,css_class,list_class,is_default,status,create_by,create_time,update_by,update_time,remark
) values
( 3,'已批准','2','provider_status','','','N','0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '已批准');
-- ------------------------add by cqw 20200627    供应商流程状态码表----------------------end

-- ------------------------add by cqw 20200627    供应商流程合格状态码表----------------------start
insert into sys_dict_type(
    dict_name,dict_type,status,create_by,create_time,update_by,update_time,remark
) values('供应商流程合格状态', 'provider_passStatus','0', 'admin', '2018-03-16 11-33-00', 'admin','2018-03-16 11-33-00', '供应商流程合格状态');


insert into sys_dict_data(
dict_sort,dict_label,dict_value,dict_type,css_class,list_class,is_default,status,create_by,create_time,update_by,update_time,remark
) values
( 1,'不合格','0','provider_passStatus','','','N','0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '不合格');

insert into sys_dict_data(
dict_sort,dict_label,dict_value,dict_type,css_class,list_class,is_default,status,create_by,create_time,update_by,update_time,remark
) values
( 2,'合格','1','provider_passStatus','','','N','0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '合格');
-- ------------------------add by cqw 20200627    供应商流程合格状态码表----------------------end


-- ------------------------add by cqw 20200628    供应商管理按钮权限----------------------start
insert into sys_menu(
    menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
    status,perms,icon,create_by,create_time,update_by,update_time,remark
) values('查询', (select * from (select menu_id from sys_menu where path = 'provider')tamp ),
'1', '#', '', 1, 'F', '0', '0', 'purchase:provider:query','#','admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');

insert into sys_menu(
    menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
    status,perms,icon,create_by,create_time,update_by,update_time,remark
) values('新增', (select * from (select menu_id from sys_menu where path = 'provider')tamp ),
'2', '#', '', 1, 'F', '0', '0', 'purchase:provider:add','#','admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');

insert into sys_menu(
    menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
    status,perms,icon,create_by,create_time,update_by,update_time,remark
) values('修改', (select * from (select menu_id from sys_menu where path = 'provider')tamp ),
'3', '#', '', 1, 'F', '0', '0', 'purchase:provider:update','#','admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');

insert into sys_menu(
    menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
    status,perms,icon,create_by,create_time,update_by,update_time,remark
) values('删除', (select * from (select menu_id from sys_menu where path = 'provider')tamp ),
'4', '#', '', 1, 'F', '0', '0', 'purchase:provider:delete','#','admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');

insert into sys_menu(
    menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
    status,perms,icon,create_by,create_time,update_by,update_time,remark
) values('查看', (select * from (select menu_id from sys_menu where path = 'provider')tamp ),
'5', '#', '', 1, 'F', '0', '0', 'purchase:provider:view','#','admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');

insert into sys_menu(
    menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
    status,perms,icon,create_by,create_time,update_by,update_time,remark
) values('提交', (select * from (select menu_id from sys_menu where path = 'provider')tamp ),
'6', '#', '', 1, 'F', '0', '0', 'purchase:provider:submit','#','admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- ------------------------add by cqw 20200628    供应商管理按钮权限----------------------end


-- ------------------------add by cqw 20200629    新增仓库管理菜单----------------------start
insert into sys_menu(
menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
status,perms,icon,create_by,create_time,update_by,update_time,remark
) values
('仓库管理', (select * from (select menu_id from sys_menu where path = 'product')tamp ), '3', 'store',
'product/store/index',   1, 'C', '0', '0', 'product:store:list', 'tab',   'admin', '2020-06-29 11-33-00', 'admin', '2020-06-29 11-33-00', '仓库管理菜单');
-- -----------------------add by cqw 20200629    新增仓库管理菜单----------------------end


-- ------------------------add by cqw 20200624    仓库表----------------------start
drop table if exists pro_store;
create table pro_store (
  id                bigint(20)      not null auto_increment     comment 'ID',
  store_num         varchar(50)                                 comment '仓库编码',
  store_name        varchar(500)                                comment '仓库名称',
  store_plot        int(11)                                     comment '仓库容量',
  store_address     varchar(500)                                comment '仓库地址',
  create_by         varchar(64)                                 comment '创建者',
  create_time       datetime                                    comment '创建时间',
  update_by         varchar(64)                                 comment '更新者',
  update_time       datetime                                    comment '更新时间',
  remark            varchar(500)                                comment '备注',
  primary key (id)
) engine=innodb  comment = '仓库表';
-- ------------------------add by cqw 20200624    仓库表----------------------end

-- ------------------------add by cqw 20200714    供应商审批菜单----------------------start
insert into sys_menu(
    menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
    status,perms,icon,create_by,create_time,update_by,update_time,remark
) values
('供应商审批', (select * from (select menu_id from sys_menu where path = 'purchase')tamp ), '2', 'providerApprove',
 'purchase/providerapprove/index',   1, 'C', '0', '0', 'purchase:providerapprove:list', 'post',   'admin', '2020-07-14 11-33-00', 'admin', '2020-07-14 11-33-00', '供应商审批菜单');
-- -----------------------add by cqw 20200714    供应商审批菜单----------------------end


-- ------------------------add by cqw 20200809   供应商审批意见码表----------------------start
insert into sys_dict_type(
    dict_name,dict_type,status,create_by,create_time,update_by,update_time,remark
) values('供应商审批意见', 'provider_ApproveOpinion','0', 'admin', '2018-03-16 11-33-00', 'admin','2018-03-16 11-33-00', '供应商流程状态');

insert into sys_dict_data(
dict_sort,dict_label,dict_value,dict_type,css_class,list_class,is_default,status,create_by,create_time,update_by,update_time,remark
) values
( 1,'同意','0','provider_ApproveOpinion','','','N','0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '同意');

insert into sys_dict_data(
dict_sort,dict_label,dict_value,dict_type,css_class,list_class,is_default,status,create_by,create_time,update_by,update_time,remark
) values
( 1,'不同意','1','provider_ApproveOpinion','','','N','0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '不同意');
-- ------------------------add by cqw 20200809   供应商审批意见码表----------------------end
































-- ------------------------add by cqw 20200714    待办事项表----------------------start
drop table if exists pro_store;
create table pro_store (
  id                bigint(20)      not null auto_increment     comment 'ID',
  module            varchar(50)                                 comment '模块',
  content        varchar(500)                                   comment '内容',
  task_id        varchar(50)                                    comment '任务id',
  business_id     varchar(500)                                  comment '业务id',



  create_by         varchar(64)                                 comment '创建者',
  create_time       datetime                                    comment '创建时间',
  update_by         varchar(64)                                 comment '更新者',
  update_time       datetime                                    comment '更新时间',
  remark            varchar(500)                                comment '备注',
  primary key (id)
) engine=innodb  comment = '仓库表';
-- ------------------------add by cqw 20200714    待办事项表----------------------end

-- ------------------------add by cqw 20200628    新增件号定义菜单----------------------start
insert into sys_menu(
menu_name,parent_id,order_num,path,component,is_frame,menu_type,visible,
status,perms,icon,create_by,create_time,update_by,update_time,remark
) values
('件号定义', (select * from (select menu_id from sys_menu where path = 'product')tamp ), '2', 'productNum',
'product/productNum/index',   1, 'C', '0', '0', 'product:productNum:list', 'edit',   'admin', '2020-06-24 11-33-00', 'admin', '2020-06-24 11-33-00', '件号定义菜单');
-- -----------------------add by cqw 20200628    新增件号定义菜单----------------------end


-- ------------------------add by cqw 20200624    库存表----------------------start
drop table if exists pro_inventory;
create table pro_inventory (
  id           bigint(20)      not null auto_increment    comment 'ID',
  bar_code         varchar(50)     not null                   comment '条形码',
  product_name         varchar(500)      default null                  comment '产品名称',
  quantity         int(11)          default 0                  comment '库存数量',
  specification     varchar(200)    default ''                 comment '规格',
  size         varchar(255)    default ''               comment '尺寸',
  color          varchar(200)         default ''                  comment '颜色',
  cost         decimal(20,0)                         comment '不含税成本价格',
  tax_cost         decimal(20,0)                         comment '含税成本价格',
  sell_price           decimal(20,0)         default 0                  comment '菜单状态（0显示 1隐藏）',
  status            char(1)         default 0                  comment '菜单状态（0正常 1停用）',
  perms             varchar(100)    default null               comment '权限标识',
  icon              varchar(100)    default '#'                comment '菜单图标',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time       datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(500)    default ''                 comment '备注',
  primary key (id)
) engine=innodb  comment = '库存表';
-- ------------------------add by cqw 20200624    库存表----------------------end











