package com.babylove.common.sms;

import com.babylove.common.utils.HttpUtils;
import com.babylove.framework.config.properties.SmsProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.common.sms
 * @Author: caoqingwen
 * @CreateTime: 2020-12-19 15:41
 * @Description: 短信发送工具类
 */

@Slf4j
@EnableConfigurationProperties(SmsProperties.class)
@Component
public class SmsSend {

    @Autowired
    private SmsProperties smsProperties;




    /*@Value("${smsWj.url}")
    public String url;*/


    /**
     * 发送短信(网建短信通)
     * @param mobile
     * @param content
     */
    public void sendMsgWj(String mobile, String content,String endCoding) {
        HttpClient client  = new HttpClient();
        System.out.println(smsProperties);
        //int a = 10/0;
        //smsProperties.getUrl()
        PostMethod post = new PostMethod("http://utf8.api.smschinese.cn/");
        //在头文件中设置转码
        post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset="+endCoding);
        NameValuePair[] data = { new NameValuePair("Uid", "曹家三少"), // 注册的用户名
                new NameValuePair("Key", "d41d8cd98f00b204e980"), // 注册成功后,登录网站使用的密钥
                new NameValuePair("smsMob", mobile), // 手机号码
                new NameValuePair("smsText", content) };//设置短信内容
        post.setRequestBody(data);
        try {
            client.executeMethod(post);
            String body = post.getResponseBodyAsString();
            log.info("短信发送返回：{}",body);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * 发送短信(阿里云)
     * @param mobile
     * @param
     */
    public void sendMsgAli(String mobile) {
        String host = "http://yzx.market.alicloudapi.com";
        String path = "/yzx/sendSms";
        String method = "POST";
        //你自己的AppCode
        String appcode = "1afe2ca6579041738377fa805e155157";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("mobile", "15026648994");
        //querys.put("param", "code:1234");
        //querys.put("param", "这里填写你和商家定义的变量名称和变量值填写格式看上一行代码");
        querys.put("param", "code:1234");
        //querys.put("tpl_id", "这里填写你和商家商议的模板");
        querys.put("tpl_id", "TP1710262");
        Map<String, String> bodys = new HashMap<String, String>();


        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            System.out.println(response.toString());
            //获取response的body
            System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
