package com.babylove.framework.aspectj.lang.enums;

/**
 * 操作状态
 * 
 * @author babylove
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
