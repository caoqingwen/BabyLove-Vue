package com.babylove.framework.aspectj.lang.enums;

/**
 * 数据源
 * 
 * @author babylove
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
