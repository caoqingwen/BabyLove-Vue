package com.babylove.framework.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.common.sms.properties
 * @Author: caoqingwen
 * @CreateTime: 2020-12-19 15:44
 * @Description: 短信配置类
 */
@Component
@ConfigurationProperties(prefix = "sms")
public class SmsProperties {


    private String url;


    private String uid;


    private String key;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
