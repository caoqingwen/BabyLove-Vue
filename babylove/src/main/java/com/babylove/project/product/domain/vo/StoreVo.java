package com.babylove.project.product.domain.vo;

import com.babylove.framework.web.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.product.domain.vo
 * @Author: caoqingwen
 * @CreateTime: 2020-07-03 10:57
 * @Description:
 */
@Data
public class StoreVo extends BaseEntity {


    private static final long serialVersionUID = 8459199176933397262L;

    private Long id;

    private String storeNum;

    @NotBlank(message = "仓库名称不能为空")
    private String storeName;

    @NotNull(message = "仓库容量不能为空")
    private Integer storePlot;

    @NotBlank(message = "仓库地址不能为空")
    private String storeAddress;

    private String optByName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date optDate;

}
