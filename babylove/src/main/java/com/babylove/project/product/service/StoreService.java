package com.babylove.project.product.service;

import com.babylove.project.product.domain.Store;
import com.babylove.project.product.domain.vo.StoreVo;

import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.product.service
 * @Author: caoqingwen
 * @CreateTime: 2020-06-30 23:28
 * @Description: 仓库Service
 */
public interface StoreService {


    /**
     * 新增、修改仓库
     * @param store
     * @return
     */
    int saveStore(Store store);

    /**
     * 查询仓库列表
     * @param req
     * @return
     */
    List<StoreVo> selectStoreList(StoreVo req);

    /**
     * 删除仓库
     * @param id
     * @return
     */
    int deleteStore(String id);
}
