package com.babylove.project.product.mapper;

import com.babylove.project.product.domain.Store;
import com.babylove.project.product.domain.vo.StoreVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.product.mapper
 * @Author: caoqingwen
 * @CreateTime: 2020-07-02 12:08
 * @Description: 仓库mapper
 */
@Mapper
public interface StoreMapper {


    /**
     * 查询最大仓库编码啊
     *
     * @return
     */
    String getMaxStoreNum();

    /**
     * 新增仓库
     *
     * @param store
     * @return
     */
    int insertStore(Store store);

    /**
     * 查询仓库列表
     *
     * @param req
     * @return
     */
    List<StoreVo> selectStoreList(StoreVo req);

    /**
     * 修改仓库
     *
     * @param store
     * @return
     */
    int updateStore(Store store);

    /**
     * 删除仓库
     *
     * @param id
     * @return
     */
    int deleteStore(@Param("id") String id);
}
