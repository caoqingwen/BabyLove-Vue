package com.babylove.project.product.domain;

import com.babylove.framework.web.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.product.domain
 * @Author: caoqingwen
 * @CreateTime: 2020-06-30 23:53
 * @Description: 仓库
 */
@Data
public class Store extends BaseEntity {

    private static final long serialVersionUID = 8459199176933397262L;

    private Long id;

    private String storeNum;

    @NotBlank(message = "仓库名称不能为空")
    private String storeName;

    @NotNull(message = "仓库容量不能为空")
    private Integer storePlot;

    @NotBlank(message = "仓库地址不能为空")
    private String storeAddress;


}
