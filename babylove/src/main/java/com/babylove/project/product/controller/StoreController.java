package com.babylove.project.product.controller;

import com.babylove.framework.web.controller.BaseController;
import com.babylove.framework.web.domain.AjaxResult;
import com.babylove.framework.web.page.TableDataInfo;
import com.babylove.project.product.domain.Store;
import com.babylove.project.product.domain.vo.StoreVo;
import com.babylove.project.product.service.StoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.product.controller
 * @Author: caoqingwen
 * @CreateTime: 2020-06-30 23:24
 * @Description: 仓库Api
 */
@RestController
@RequestMapping("/product/store")
@Api(value = "仓库Controller", tags = {"仓库接口"})
public class StoreController extends BaseController {

    @Autowired
    private StoreService storeService;


    @GetMapping("/list")
    @ApiOperation(value = "仓库列表查询接口", notes = "仓库列表查询接口")
    public TableDataInfo getList(StoreVo req) {
        startPage();
        List<StoreVo> list = storeService.selectStoreList(req);
        return getDataTable(list);
    }


    @PostMapping("save")
    @ApiOperation(value = "新增仓库接口", notes = "新增仓库接口")
    public AjaxResult saveStore(@RequestBody @Validated Store store) {
        return toAjax(storeService.saveStore(store));
    }


    @PostMapping("/deleteStore")
    @ApiOperation(value = "删除仓库接口",notes = "删除仓库接口")
    public AjaxResult deleteStore(@RequestBody String id){
        return toAjax(storeService.deleteStore(id));
    }

}
