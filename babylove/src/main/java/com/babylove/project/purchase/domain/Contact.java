package com.babylove.project.purchase.domain;

import com.babylove.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.purchase.domain
 * @Author: caoqingwen
 * @CreateTime: 2020-06-25 01:39
 * @Description: 供应商联系人
 */
@Data
public class Contact extends BaseEntity {

    private Long id;

    private String contactName;

    private String contactPos;

    private String contactPhone;

    private Long providerId;


}
