package com.babylove.project.purchase.domain.resp;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.purchase.domain.resp
 * @Author: caoqingwen
 * @CreateTime: 2020-07-27 14:49
 * @Description: 工作流待办任务返回类
 */
@Data
public class TaskEntityResp {

    private String id;

    private String name;

    private String assignee;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


}
