package com.babylove.project.purchase.controller;

import com.babylove.framework.web.controller.BaseController;
import com.babylove.framework.web.domain.AjaxResult;
import com.babylove.framework.web.page.TableDataInfo;
import com.babylove.project.purchase.domain.req.ProviderAddReq;
import com.babylove.project.purchase.domain.vo.ProviderVo;
import com.babylove.project.purchase.service.IPurProviderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.purchase.controller
 * @Author: caoqingwen
 * @CreateTime: 2020-06-25 01:15
 * @Description: 供应商管理API
 */
@RestController
@Api(value = "供应商Controller", tags = {"供应商接口"})
@RequestMapping("/purchase/provider")
public class PurProviderController extends BaseController {

    @Autowired
    private IPurProviderService purProviderService;


    @GetMapping("/list")
    @ApiOperation(value = "供应商列表查询", notes = "供应商列表查询")
    public TableDataInfo getList(ProviderVo req) {
        startPage();
        List<ProviderVo> list = purProviderService.selectProviderList(req);
        return getDataTable(list);
    }


    @PostMapping("/save")
    @ApiOperation(value = "新增/修改供应商", notes = "新增/修改供应商")
    public AjaxResult saveProvider(@RequestBody @Validated ProviderAddReq req) {
        purProviderService.saveProvider(req);
        return AjaxResult.success();
    }


    @PostMapping("/getProviderById")
    @ApiOperation(value = "根据id查询供应商", notes = "根据id查询供应商")
    public AjaxResult getProviderById(@RequestBody Long id) {
        return AjaxResult.success(purProviderService.getProviderById(id));
    }


    @PostMapping("/deleteProvider")
    @ApiOperation(value = "删除供应商", notes = "删除供应商")
    public AjaxResult deleteProvider(@RequestBody Long id) {
        purProviderService.deleteProvider(id);
        return AjaxResult.success();
    }


    @PostMapping("/startProviderProcess")
    @ApiOperation(value = "提交申请接口", notes = "提交申请接口")
    public AjaxResult startProviderProcess(@RequestBody ProviderVo vo) {
        purProviderService.startProviderProcess(vo);
        return AjaxResult.success();
    }


    /**
     * 查询代办事项
     *
     * @return
     */
    @PostMapping("/getProviderApproveList")
    public AjaxResult getProviderApproveList() {
        return AjaxResult.success(purProviderService.getProviderApproveList());
    }

    /**
     * 根据任务id，查询供应商信息
     *
     * @param id
     * @return
     */
    @PostMapping("/getProviderByTaskId")
    public AjaxResult getProviderByTaskId(@RequestBody String id) {
        return AjaxResult.success(purProviderService.getProviderByTaskId(id));
    }

}
