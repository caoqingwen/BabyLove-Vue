package com.babylove.project.purchase.domain.req;

import com.babylove.framework.web.domain.BaseEntity;
import com.babylove.project.purchase.domain.Contact;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.purchase.domain.req
 * @Author: caoqingwen
 * @CreateTime: 2020-06-27 15:36
 * @Description: 供应商新增
 */
@Data
public class ProviderAddReq extends BaseEntity {

    private Long id;

    @Size(min = 0,max = 200,message = "供应商名称不能超过200字符")
    private String providerNum;

    @Size(min = 0,max = 200,message = "供应商名称不能超过200字符")
    @NotBlank(message = "供应商名称不能为空")
    private String providerName;

    @Size(min = 0,max = 200,message = "供应商类型不能超过200字符")
    @NotBlank(message = "供应商类型不能为空")
    private String providerType;

    private String status;

    @NotNull(message = "有效期不能为空")
    private Date validDate;

    private String passStatus;

    private List<Contact> contactList;

    private String optByName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date optDate;
}
