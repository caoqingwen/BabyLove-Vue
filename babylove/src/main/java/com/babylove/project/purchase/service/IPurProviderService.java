package com.babylove.project.purchase.service;

import com.babylove.project.purchase.domain.req.ProviderAddReq;
import com.babylove.project.purchase.domain.resp.TaskEntityResp;
import com.babylove.project.purchase.domain.vo.ProviderVo;

import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.purchase.service
 * @Author: caoqingwen
 * @CreateTime: 2020-06-25 14:39
 * @Description: 供应商
 */
public interface IPurProviderService {


    /**
     * 新增、修改供应商
     * @param req
     */
    void saveProvider(ProviderAddReq req);

    /**
     * 供应商列表查询
     * @param req
     * @return
     */
    List<ProviderVo> selectProviderList(ProviderVo req);

    /**
     * 根据id查询供应商
     * @param id
     * @return
     */
    ProviderAddReq getProviderById(Long id);

    /**
     * 删除供应商
     * @param id
     */
    void deleteProvider(Long id);

    /**
     * 提交申请
     * @param vo
     */
    void startProviderProcess(ProviderVo vo);

    /**
     * 获取待办任务
     * @return
     */
    List<TaskEntityResp> getProviderApproveList();

    /**
     * 根据任务id查询供应商信息
     * @param id
     */
    ProviderVo getProviderByTaskId(String id);
}
