package com.babylove.project.purchase.mapper;

import com.babylove.project.purchase.domain.Contact;
import com.babylove.project.purchase.domain.req.ProviderAddReq;
import com.babylove.project.purchase.domain.vo.ProviderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.purchase.mapper
 * @Author: caoqingwen
 * @CreateTime: 2020-06-25 14:45
 * @Description: 供应商管理
 */
@Mapper
public interface PurProviderMapper {

    /**
     * 新增供应商
     * @param req
     * @return
     */
    Integer insertProvider(ProviderAddReq req);

    /**
     * 新增供应商联系人
     * @param contactList
     * @return
     */
    Integer insertProviderContact(@Param("list") List<Contact> contactList);

    /**
     * 查询最大供应商编号
     * @return
     */
    String getMaxProviderNum();

    /**
     * 修改供应商
     * @param req
     * @return
     */
    Integer updateProvider(ProviderAddReq req);

    /**
     * 删除联系人
     * @param id
     * @return
     */
    Integer deleteContactByProviderId(Long id);

    /**
     * 校验供应商名称是否唯一
     * @param providerName
     * @return
     */
    int selectCountByProviderNum(String providerName);

    /**
     * 供应商列表查询
     * @param req
     * @return
     */
    List<ProviderVo> selectProviderList(ProviderVo req);

    /**
     * 查询供应商联系人
     * @param id
     * @return
     */
    List<Contact> selectContactList(Long id);

    /**
     * 删除供应商
     * @param id
     * @return
     */
    Integer deleteProvider(Long id);
}
