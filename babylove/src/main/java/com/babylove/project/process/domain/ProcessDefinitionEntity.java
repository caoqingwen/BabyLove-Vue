package com.babylove.project.process.domain;

import lombok.Data;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.process.domain
 * @Author: caoqingwen
 * @CreateTime: 2020-07-05 16:31
 * @Description:
 */
@Data
public class ProcessDefinitionEntity {


    private String id;

    private String name;

    private String key;

    private Integer version;

    private String deploymentId;

    private String resourceName;

    private String diagramResourceName;





}
