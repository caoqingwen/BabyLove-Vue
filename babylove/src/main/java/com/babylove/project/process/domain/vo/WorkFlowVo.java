package com.babylove.project.process.domain.vo;

import com.babylove.framework.web.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.process.domain.vo
 * @Author: caoqingwen
 * @CreateTime: 2020-07-05 00:17
 * @Description:
 */
@Data
public class WorkFlowVo extends BaseEntity {


    private Integer pageSize;

    private Integer pageNum;

    @ApiModelProperty(value = "流程部署名称")
    private String deploymentName;


}
