package com.babylove.project.process.controller;

import com.babylove.common.utils.sign.Base64;
import com.babylove.framework.web.controller.BaseController;
import com.babylove.framework.web.domain.AjaxResult;
import com.babylove.framework.web.page.TableDataInfo;
import com.babylove.project.process.domain.DeploymentEntity;
import com.babylove.project.process.domain.ProcessDefinitionEntity;
import com.babylove.project.process.domain.vo.WorkFlowVo;
import com.babylove.project.process.service.WorkFlowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.process.controller
 * @Author: caoqingwen
 * @CreateTime: 2020-06-27 18:11
 * @Description: 工作流controller
 */
@RestController
@RequestMapping("/process/workFlow")
@Api(value = "流程Controller", tags = "流程管理接口")
public class WorkFlowController extends BaseController {

    @Autowired
    private WorkFlowService workFlowService;

    @GetMapping("/repositoryList")
    @ApiOperation(value = "流程部署查询接口", notes = "流程部署查询接口")
    public TableDataInfo getRepositoryList(WorkFlowVo vo) {
        startPage();
        List<DeploymentEntity> repositoryList = workFlowService.getRepositoryList(vo);
        return getDataTable(repositoryList);
    }


    @GetMapping("/processDefinitionList")
    public TableDataInfo getProcessDefinitionList(String processDefinitionId) {
        startPage();
        List<ProcessDefinitionEntity> processDefinitionList = workFlowService.getProcessDefinitionList(processDefinitionId);
        return getDataTable(processDefinitionList);
    }


    @PostMapping("/deploymentProcess")
    public AjaxResult deploymentProcess(MultipartFile file, String deploymentName) {
        workFlowService.deploymentProcess(file, deploymentName);
        return new AjaxResult();
    }

    @PostMapping("/deleteDeployment")
    @ApiOperation(value = "流程部署删除接口", notes = "流程部署删除接口")
    public AjaxResult deleteDeployment(@RequestBody DeploymentEntity deploymentEntity) {
        workFlowService.deleteDeployment(deploymentEntity);
        return new AjaxResult();
    }


    @GetMapping("/getProcessPic/{id}")
    public AjaxResult getProcessPic(@PathVariable("id") String id, HttpServletResponse response) {
        InputStream stream = workFlowService.getProcessPic(id);
        //读出去
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((len = stream.read(buffer))!= -1){
                outputStream.write(buffer,0,len);
            }
            AjaxResult ajaxResult = new AjaxResult();
            ajaxResult.put("img", Base64.encode(outputStream.toByteArray()));
            return ajaxResult;

        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.error(e.getMessage());
        }
    }

}
