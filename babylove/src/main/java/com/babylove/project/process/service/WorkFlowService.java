package com.babylove.project.process.service;

import com.babylove.project.process.domain.DeploymentEntity;
import com.babylove.project.process.domain.ProcessDefinitionEntity;
import com.babylove.project.process.domain.vo.WorkFlowVo;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.process.service
 * @Author: caoqingwen
 * @CreateTime: 2020-07-03 20:59
 * @Description: 工作流Service
 */
public interface WorkFlowService {


    /**
     * 查询流程部署
     * @param vo
     */
    List<DeploymentEntity> getRepositoryList(WorkFlowVo vo);

    /**
     * 查询流程定义
     * @param processDefinitionId
     * @return
     */
    List<ProcessDefinitionEntity> getProcessDefinitionList(String processDefinitionId);

    /**
     * 部署流程
     * @param file
     * @param deploymentName
     */
    void deploymentProcess(MultipartFile file, String deploymentName);

    void deleteDeployment(DeploymentEntity deploymentEntity);

    /**
     * 查询流程图
     * @param id
     * @return
     */
    InputStream getProcessPic(String id);
}
