package com.babylove.project.process.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.process.domain.vo
 * @Author: caoqingwen
 * @CreateTime: 2020-07-05 00:24
 * @Description: 流程部署类
 */
@Data
public class DeploymentEntity {


    private String id;

    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date deploymentTime;


}
