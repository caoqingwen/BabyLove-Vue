package com.babylove.project.sms.Controller;

import com.babylove.common.sms.SmsSend;
import com.babylove.framework.web.controller.BaseController;
import com.babylove.framework.web.domain.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: babylove
 * @BelongsPackage: com.babylove.project.sms.Controller
 * @Author: caoqingwen
 * @CreateTime: 2020-12-19 17:06
 * @Description: 短信api
 */
@RestController
@Slf4j
@RequestMapping("/sms")
@Api(tags = "短信接口")
public class SmsController extends BaseController {

   /* @Value("${sms.url}")
    private String url;*/

    @Autowired
    private SmsSend smsSend;

    @Autowired
    private RedisTemplate redisTemplate;




    @PostMapping("/sendWj")
    @ApiOperation(value = "发送短信", notes = "发送短信接口")
    public AjaxResult sendSmsWj() {
        String content = "尊敬的赵老师：您的验证码是：1234【曹青稳】";
        smsSend.sendMsgWj("13524989207", content, "UTF-8");
        return AjaxResult.success();
    }

    @PostMapping("/sendAli")
    @ApiOperation(value = "发送短信", notes = "发送短信接口")
    public AjaxResult sendSmsAli() {
        String content = "尊敬的用户：恭喜您中一台iphone12【BabyLove公司】";
        smsSend.sendMsgAli("18055666231");
        return AjaxResult.success();
    }


    @PostMapping("/testRedis")
    @ApiOperation(value = "发送短信", notes = "发送短信接口")
    public AjaxResult testRedis() {
        final ArrayList<Object> objects = new ArrayList<>();
        objects.add(1);
        objects.add(2);
        redisTemplate.opsForList().leftPush("list1",objects);
        final List list1 = redisTemplate.opsForList().range("list2", 0, -1);
        for (Object o : list1) {
            System.out.println(o);
        }
        return AjaxResult.success();
    }


}
