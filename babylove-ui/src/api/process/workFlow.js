import request from '@/utils/request'


/*流程部署查询*/
export function getRepositoryList(data) {
  return request({
    url: '/process/workFlow/repositoryList',
    method: 'GET',
    params: data
  })
}


export function getProcessDefinitionList(data) {
  return request({
    url: "/process/workFlow/processDefinitionList",
    method: "GET",
    params: data
  })
}


export function deleteDeployment(data) {
  return request({
    url: "/process/workFlow/deleteDeployment",
    method: "POST",
    data: data
  })
}


export function getProcessPic(data) {
  return request({
    url: "/process/workFlow/getProcessPic/"+data,
    method: "GET"
  })
}
