import request from '@/utils/request'


export function getList(data) {
  return request({
    url: '/product/store/list',
    method: 'GET',
    params: data
  })
}


export function saveStore(data) {
  return request({
    url: '/product/store/save',
    method: 'POST',
    data: data
  })
}


export function deleteStore(data) {
  return request({
    url:"/product/store/deleteStore",
    method:"POST",
    data:data
  })
}






