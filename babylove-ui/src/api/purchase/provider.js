import request from '@/utils/request'

/*查询*/
export function getList(data) {
  return request({
    url: "/purchase/provider/list",
    method: "GET",
    params: data
  })
}


/*保存*/
export function saveProvider(data) {
  return request({
    url: '/purchase/provider/save',
    method: 'POST',
    data: data
  })
}


export function getProviderById(id) {
  return request({
    url: '/purchase/provider/getProviderById',
    method: 'POST',
    data: id
  })
}

export function deleteProvider(id) {
  return request({
    url: '/purchase/provider/deleteProvider',
    method: 'POST',
    data: id
  })
}


export function startProviderProcess(data) {
  return request({
    url: "/purchase/provider/startProviderProcess",
    method: "POST",
    data: data
  })
}

/**
 * 供应商审批数据查询
 * @param data
 */
export function getProviderApproveList(data) {
  return request({
    url: "/purchase/provider/getProviderApproveList",
    method: "POST",
    data: data
  })
}

/**
 * 根据任务id查询供应商信息
 * @param data
 */
export function getProviderByTaskId(data) {
  return request({
    url:"/purchase/provider/getProviderByTaskId",
    method:"POST",
    data:data
  })
}



